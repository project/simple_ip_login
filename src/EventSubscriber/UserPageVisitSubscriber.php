<?php

namespace Drupal\simple_ip_login\EventSubscriber;

use Drupal\simple_ip_login\Controller\LoginController;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpFoundation\Session\SessionInterface;
use Symfony\Contracts\EventDispatcher\Event;

/**
 * Logs out users if there is no matching IP wildcard.
 */
class UserPageVisitSubscriber implements EventSubscriberInterface {

  /**
   * The session service.
   *
   * @var \Symfony\Component\HttpFoundation\Session\SessionInterface
   */
  private $sessionService;

  /**
   * Constructs a new UserPageVisitSubscriber object.
   *
   * @param \Symfony\Component\HttpFoundation\Session\SessionInterface $sessionService
   *   The current session.
   */
  public function __construct(SessionInterface $sessionService) {
    $this->sessionService = $sessionService;
  }

  /**
   * {@inheritdoc}
   */
  public static function getSubscribedEvents() {
    $events['kernel.request'] = ['autoLogout'];

    return $events;
  }

  /**
   * This method is called whenever the kernel.request event is dispatched.
   *
   * @param \Symfony\Contracts\EventDispatcher\Event $event
   *   The kernel request event.
   */
  public function autoLogout(Event $event): void {
    $isAutoLogin = (bool) $this->sessionService->get('autologin');
    if ($isAutoLogin && !LoginController::isWildcardUser()) {
      user_logout();
    }
  }

}
