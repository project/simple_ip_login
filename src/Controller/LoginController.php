<?php

namespace Drupal\simple_ip_login\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Link;
use Drupal\simple_ip_login\Entity\IPWildcard;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\Session\Session;

/**
 * Facilitates the IP-based login.
 */
class LoginController extends ControllerBase {

  /**
   * The current session.
   *
   * @var \Symfony\Component\HttpFoundation\Session\Session
   */
  private $session;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('session')
    );
  }

  /**
   * LoginController constructor.
   *
   * @param \Symfony\Component\HttpFoundation\Session\Session $session
   *   The current session.
   */
  public function __construct(Session $session) {
    $this->session = $session;
  }

  /**
   * Logs user in or show an error message.
   *
   * @return mixed
   *   Return Hello string.
   */
  public function iplogin() {

    if ($userID = self::isWildcardUser()) {
      user_login_finalize($this->entityTypeManager()->getStorage('user')->load($userID));
      $this->session->set('autologin', TRUE);

      return $this->redirect('<front>');
    }

    return [
      'failed' =>
        [
          '#markup' => $this->t('Login failed...') . '<br />',
        ],
      'link' => Link::createFromRoute('Back to login page', 'user.login')
        ->toRenderable(),
    ];
  }

  /**
   * Checks if user has a ip login.
   *
   * @return bool|int
   *   The ID of a matching user, or FALSE.
   */
  public static function isWildcardUser() {
    $ipWildcards = IPWildcard::loadMultiple();

    foreach ($ipWildcards as $ipWildcard) {
      if ((bool) preg_match($ipWildcard->getIpWildcard(), \Drupal::request()
        ->getClientIp())) {
        return $ipWildcard->getUserId();
      }
    }
    return FALSE;
  }

}
